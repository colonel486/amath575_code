function symtest1
%SYMTEST1 Test for symplectic methods.
%  SYMTEST1 Integrates x" + sin(x) = 0 using SEEQ, SEEP, GLS and ODE4 solvers.

%  Francisco J. Beron-Vera, 12-Feb-2005
%  $Revision: 1.3 $  $Date: 20-Jul-2006 01:30:25 $

figure(1)
clf

% Time span.
t=0:.01:200;

% Initial conditions.
u0=[1:5]';  
v0=u0;

% % SEEQ
% tic, [q p]=seeq(@Xq,@Xp,t,q0,p0); et=toc;
% subplot(131)
% plot(mod(q,4*pi),p,'.b','MarkerS',1)
% axis([pi 3*pi -5 5])%, axis square
% xlabel('q'), ylabel('p')
% title(['SEEQ (elapsed time = ' num2str(et) ')']) 
% disp('SEIQ done.')

% SEEP
tic, [u v]=lotka(t,u0,v0); et=toc;
subplot(121)
plot(u,v,'.b','MarkerS',1)
axis([0 12 0 12])%, axis square
xlabel('u'), ylabel('v')
title(['Symplectic Euler (elapsed time = ' num2str(et) ')']) 
grid on
disp('SEEP done.')

tic, [u v]=lotkaEx(t,u0,v0); et=toc;
subplot(122)
plot(u,v,'.b','MarkerS',1)
axis([0 12 0 12])%, axis square
xlabel('u'), ylabel('v')
title(['Explicit Euler (elapsed time = ' num2str(et) ')']) 
grid on
disp('SEEP done.')

% GLS
% z0=[q0';p0']; z0=z0(:);
% tic, z=gls(@X,@DX,t,z0,3,t,1e-3,5); et=toc;
% q=z(2:end,1:2:end-1);
% p=z(2:end,2:2:end);
% subplot(223)
% plot(mod(q,4*pi),p,'.b','MarkerS',1)
% axis([pi 3*pi -5 5]), axis square
% xlabel('$q$'), ylabel('$p$')
% title(['GLS (elapsed time = ' num2str(et) ')']) 
% disp('GLS done.')

% ODE45
% z0=[u0';v0']; z0=z0(:);
% tic, [to,z]=ode45(@X,t,z0); et=toc;
% q=z(2:end,1:2:end-1);
% p=z(2:end,2:2:end);
% subplot(122)
% plot(q,p,'.b','MarkerS',1)
% axis([0 15 0 15])%, axis square
% xlabel('u'), ylabel('v')
% title(['ODE45 (elapsed time = ' num2str(et) ')']) 
% disp('ODE45 done.')

%--------------------------------------------------------------------------
function out=Xq(p,q)
out=q*(p-1);
%--------------------------------------------------------------------------
function out=Xp(p,q)
out=p*(2-q); 
%--------------------------------------------------------------------------
function out=X(t,z)
n=length(z);
q=z(1:2:end-1);
p=z(2:2:end);
Hq=q.*(p-2);
Hp=p.*(1-q);
out=[Hp;Hq];
out=reshape(out,[n/2 2])';
out=out(:);
%--------------------------------------------------------------------------
function out=DX(t,z)
n=length(z);
q=z(1:2:end-1);
Hqq=cos(q);
Hqp=zeros(n/2,1);
Hpp=ones(n/2,1); 
out=[diag(Hqp) diag(Hpp); -diag(Hqq) -diag(Hqp)];
