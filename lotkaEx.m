function [u,v] = lotka(tspan, u0,v0)

N = length(u0);
Nt = length(tspan);
hs = diff(tspan);
u = zeros(N,Nt); u(:,1)=u0;
v = zeros(N,Nt); v(:,1)=v0;

for nt = 2:Nt
   h = hs(nt-1);
   u(:,nt) = u(:,nt-1) + h * (u(:,nt-1).*(v(:,nt-1)-2));%feval(dqdt, p(:,nt-1), q(:,nt-1));
   v(:,nt) = v(:,nt-1) + h * (v(:,nt-1).*(1-u(:,nt-1)));
end

u = u.';
v = v.';